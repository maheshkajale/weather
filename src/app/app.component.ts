import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Weather Forecasts';
  cities = [
    {city: 'London', country: 'uk'},
    {city: 'Rome', country: 'it'},
    {city: 'Paris', country: 'fr'},
    {city: 'Madrid', country: 'es'},
    {city: 'Berlin', country: 'de'},
  ];
  public forecasts: any = [];
  apiData = {
    baseUrl: 'http://api.openweathermap.org',
    appId: '3d8b309701a13f65b660fa2c64cdc517',
  };

  constructor(private http: HttpClient) {}
  ngOnInit() {
    console.log(111);
    this.cities.forEach(city => {
      const qryParam = `q=${city.city},${city.country}&appid=${this.apiData.appId}`;
      const url = `${this.apiData.baseUrl}/data/2.5/weather?${qryParam}`;
      this.http.get(url).subscribe((res: any) => {
        if (res) {
          const data: any = {
            city: city.city,
            temperature: res.main.temp,
            sunrise: res.sys.sunrise,
            sunset: res.sys.sunset,
            forecastList: []
          };
          const forecastUrl = `${this.apiData.baseUrl}/data/2.5/forecast?${qryParam}&cnt=5`;
          this.http.get(forecastUrl).subscribe((response: any) => {
            if (response) {
              data.forecastList = response.list
            }
          });
          this.forecasts.push(data);
        }
      });
    });
  }
}
