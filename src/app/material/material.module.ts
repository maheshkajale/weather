import { NgModule } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import {  MatExpansionModule } from '@angular/material/expansion';
import {  MatChipsModule } from '@angular/material/chips';
import {  MatListModule } from '@angular/material/list';
import {  MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatChipsModule,
    MatListModule,
    MatIconModule,
  ],
  exports: [
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatChipsModule,
    MatListModule,
    MatIconModule,
  ]
})

export class MaterialModule {}